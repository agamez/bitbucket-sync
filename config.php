<?php

$CONFIG = array(
	'commitsFolder' => 'commits',
	'commitsFilenamePrefix' => 'commit-',
	'automaticDeployment' => true,
	'deployBranch' => 'master',
	'apiUser' => '',
	'apiPassword' => '',
	'verbose' => true,
	'requireAuthentication' => false,
	'deployAuthKey' => '',
	'gatewayAuthKey' => '',
);

$DEPLOY = array();

// Create log file for configuration
function loginfoconfig($message) {
	global $CONFIG;
	if( $CONFIG['verbose'] ) {
		echo $message;
		// add log sync file
		$log_sync_file = 'log_sync.txt';
		file_put_contents($log_sync_file, $message, FILE_APPEND | LOCK_EX);
		flush();
	}
}

// Check GET parameters
CONFIG_PER_GET('CONFIG', 'deployBranch');
CONFIG_PER_GET('CONFIG', 'apiUser');
CONFIG_PER_GET('CONFIG', 'apiPassword');
CONFIG_PER_GET('DEPLOY', 'deploy');


// Replaces the array if there is get parameters
function CONFIG_PER_GET($SETTINGS, $key){
	GLOBAL $CONFIG;
	GLOBAL $DEPLOY;
	
	if(isset($_GET[$key]) && !empty($_GET[$key])) {
		$value = strip_tags(stripslashes(urldecode($_GET[$key])));
		echo $value;
		if($SETTINGS == 'CONFIG'){
			$CONFIG[$key] = $value;
			if($key != 'apiPassword'){
				loginfoconfig("* Get parameter: " . $value . "\n");
			};
		};
		if($SETTINGS == 'DEPLOY'){
			// explode value deploy
			list($deply_repo, $deploy_url) = explode(":", $value);
			loginfoconfig("* GET Repository: " . $deply_repo . "\n");
			loginfoconfig("* GET url: " . $deploy_url . "\n");
			$DEPLOY = array($deply_repo => $deploy_url,);
		};
	};
}


